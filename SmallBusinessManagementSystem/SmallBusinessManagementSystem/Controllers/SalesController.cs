﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmallBusinessManagementSystem.BLL.SalesManager;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.Controllers
{
    public class SalesController : Controller
    {
        SalesManager _manager=new SalesManager();
       [HttpGet] 
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
       public ActionResult Create(Sales sales)
       {
           if (ModelState.IsValid && sales.SalesDetailses != null && sales.SalesDetailses.Count > 0)
           {
               var isAdded = _manager.Add(sales);
               if (isAdded)
               {
                   return View(sales);
               }


           }
           return View();
       }
    }
}