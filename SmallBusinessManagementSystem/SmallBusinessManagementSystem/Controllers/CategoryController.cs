﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmallBusinessManagementSystem.BLL.CategoryManager;
using SmallBusinessManagementSystem.Models.Models;


namespace SmallBusinessManagementSystem.Controllers
{
    public class CategoryController : Controller
    {
        CategoryManager _manager=new CategoryManager();
        public ActionResult Index()
        {
            return View();
        }

     
        public ActionResult GetCategories()
        {
            var categories = _manager.GetCategories();
            return Json(new { data = categories }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public ActionResult Save(int id)
        {
            var category = _manager.Save(id);
            return View(category);
        }
      
        [HttpPost]
        public ActionResult Save(Category category)
        {
            int status = 0;
            bool isSaved = false;
            if (ModelState.IsValid)
            {
                status = _manager.SaveByPost(category);
            }
            if (status > 0)
            {
                isSaved = true;
            }

            return new JsonResult { Data = new { isSaved = isSaved } };
        }

        [HttpGet]

        public ActionResult Delete(int id)
        {
            var v = _manager.Delete(id);
            if (v != null)
            {
                return View(v);
            }
            else
            {
                return HttpNotFound();
            }
            
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteCategory(int id)
        {
            bool isSaved = false;
            int status = _manager.DeleteCategory(id);
            if (status > 0)
            {
                isSaved = true;
            }
            return new JsonResult { Data = new { isSaved = isSaved } };
        }
    }
}