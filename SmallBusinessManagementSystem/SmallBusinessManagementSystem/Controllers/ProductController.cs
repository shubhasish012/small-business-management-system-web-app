﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmallBusinessManagementSystem.BLL.ProductManager;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.Controllers
{
    public class ProductController : Controller
    {
        ProductManager _manager=new ProductManager();
        static private HttpPostedFileBase image;
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetProducts()
        {
            var products = _manager.GetProducts();
            return Json(new { data = products }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public ActionResult Save(int id)
        {
            var products = _manager.Save(id);
            ViewBag.CategoryId = _manager.GetCategoryList();
           
            return View(products);
        }
        [HttpPost]
        public void Upload(Product product)
       {
         if (product.ImageFile != null && product.ImageFile.ContentLength > 0)
         {
             image = product.ImageFile;
         }
}

        [HttpPost]
        public ActionResult Save(Product product)
        {
            product.ImageFile = image;
            int status = 0;
            bool isSaved = false;
            if (ModelState.IsValid)
            {
                
                status = _manager.SaveByPost(product);
            }
            if (status > 0)
            {
                isSaved = true;
            }

            return new JsonResult { Data = new { isSaved = isSaved } };
            
        }

        [HttpGet]

        public ActionResult Delete(int id)
        {
            var v = _manager.Delete(id);
            if (v != null)
            {
                return View(v);
            }
            else
            {
                return HttpNotFound();
            }

        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteProduct(int id)
        {
            bool isSaved = false;
            int status = _manager.DeleteProduct(id);
            if (status > 0)
            {
                isSaved = true;
            }

            return new JsonResult { Data = new { isSaved = isSaved } };
        }

        [HttpGet]
        public ActionResult ProductImageView(int id)
        {
            Product product = new Product();

            //using (DbModels db = new DbModels())
            //  {
            //imageModel = db.Images.Where(x => x.ImageID == id).FirstOrDefault();
            //  }
            product = _manager.GetImageView(id);

            return View(product);
        }
    }
}