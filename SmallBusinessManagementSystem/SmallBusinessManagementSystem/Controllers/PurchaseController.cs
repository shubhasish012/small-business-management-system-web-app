﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmallBusinessManagementSystem.BLL.PurchaseManager;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.Controllers
{
    public class PurchaseController : Controller
    {
        PurchaseManager _manager=new PurchaseManager();
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(Purchase purchase)
        {
            if (ModelState.IsValid && purchase.PurchaseDetailses != null && purchase.PurchaseDetailses.Count > 0)
            {
                var isAdded = _manager.Add(purchase);
                if (isAdded)
                {
                    return View(purchase);
                }

                
            }
            return View();
        }
        
    }
}