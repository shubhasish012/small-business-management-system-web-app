﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SmallBusinessManagementSystem.Controllers
{
    public class SecurityController : Controller
    {
        // GET: Security
       [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        
        [Authorize]
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult My()
        {
            return View();
        }

[HttpPost]
        public ActionResult CheckUser()
        {
    if (Request.Form["Email"] == "user@gmail.com" && Request.Form["Password"] == "Password")
    {
        FormsAuthentication.SetAuthCookie(Request.Form["Email"],true);
        return RedirectToAction("Home");
    }
            else
            {
                return View("Login");
            }
        }
    }
}