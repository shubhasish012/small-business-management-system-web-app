﻿$("#AddButton").click(function() {
    CreateRowForPurchase();
});

function CreateRowForPurchase() {

    var selectedItem = GetSelectedItem();

    //Check Last Row Index
    var index = $("#PurchaseDetailsTable").children("tr").length;
    var sl = index;

    //For Model List<Property> Binding For MVC
    var indexTd = "<td style='display:none'><input type='hidden' id='Index" + index + "' name='PurchaseDetailses.Index' value='" + index + "' /> </td>";

    //For Serial No For UI
    var slTd = "<td id='Sl" + index + "'> " + (++sl) + " </td>";

    var ProductIdTd = "<td> <input type='hidden' id='PurchaseProductId" + index + "'  name='PurchaseDetailses[" + index + "].ProductId' value='" + selectedItem.PurchaseProductId + "' /> " + selectedItem.PurchaseProductId + " </td>";
    var ProductCodeTd = "<td> <input type='hidden' id='PurchaseProductCode" + index + "'  name='PurchaseDetailses[" + index + "].ProductCode' value='" + selectedItem.ProductCode + "' /> " + selectedItem.PurchaseProductCode + " </td>";
    var ManufacturedDateTd = "<td> <input type='hidden' id='PurchaseManufacturedDate" + index + "'  name='PurchaseDetailses[" + index + "].ManufacturedDate' value='" + selectedItem.ManufacturedDate + "' /> " + selectedItem.PurchaseManufacturedDate + " </td>";
    var ExpireDateTd = "<td> <input type='hidden' id='PurchaseExpireDate" + index + "'  name='PurchaseDetailses[" + index + "].ExpireDate' value='" + selectedItem.ExpireDate + "' /> " + selectedItem.PurchaseExpireDate + " </td>";
    var PurchaseQuantityTd = "<td> <input type='hidden' id='PurchaseQty" + index + "'  name='PurchaseDetailses[" + index + "].PurchaseQuantity' value='" + selectedItem.PurchaseQuantity + "' /> " + selectedItem.PurchaseQty + " </td>";
    var UnitPriceTd = "<td> <input type='hidden' id='PurchaseUnitPrice" + index + "'  name='PurchaseDetailses[" + index + "].UnitPrice' value='" + selectedItem.UnitPrice + "' /> " + selectedItem.PurchaseUnitPrice + " </td>";
    var TotalPriceTd = "<td> <input type='hidden' id='PurchaseTotalPrice" + index + "'  name='PurchaseDetailses[" + index + "].TotalPrice' value='" + selectedItem.TotalPrice + "' /> " + selectedItem.PurchaseTotalPrice + " </td>";
    var PreviousPriceTd = "<td> <input type='hidden' id='PurchasePreviousPrice" + index + "'  name='PurchaseDetailses[" + index + "].PreviousCostPrice' value='" + selectedItem.PreviousCostPrice + "' /> " + selectedItem.PurchasePreviousPrice + " </td>";
    var PreviousMRPTd = "<td> <input type='hidden' id='PurchasePreviousMRP" + index + "'  name='PurchaseDetailses[" + index + "].PreviousMRP' value='" + selectedItem.PreviousMRP + "' /> " + selectedItem.PurchasePreviousMRP + " </td>";
    var NewCostPriceTd = "<td> <input type='hidden' id='PurchaseNewCostPrice" + index + "'  name='PurchaseDetailses[" + index + "].NewCostPrice' value='" + selectedItem.NewCostPrice + "' /> " + selectedItem.PurchaseNewCostPrice + " </td>";
    var NewMRPTd = "<td> <input type='hidden' id='PurchaseNewMRP" + index + "'  name='PurchaseDetailses[" + index + "].NewMRP' value='" + selectedItem.NewMRP + "' /> " + selectedItem.PurchaseNewMRP + " </td>";

    var newRow = "<tr>" + indexTd + slTd + ProductIdTd + ProductCodeTd + ManufacturedDateTd + ExpireDateTd + PurchaseQuantityTd + UnitPriceTd + TotalPriceTd + PreviousPriceTd + PreviousMRPTd + NewCostPriceTd + NewMRPTd + " </tr>";

    $("#PurchaseDetailsTable").append(newRow);
    $("#PurchaseProductId").val("");
    $("#PurchaseProductCode").val("");
    $("#PurchaseManufacturedDate").val("");
    $("#PurchaseQty").val("");
    $("#PurchaseUnitPrice").val("");
    $("#PurchaseTotalPrice").val("");
    $("#PurchasePreviousPrice").val("");
    $("#PurchaseNewCostPrice").val("");
    $("#PurchaseNewMRP").val("");

}

function GetSelectedItem() {
    var PurchaseProductId = $("#PurchaseProductId").val();
    var PurchaseProductCode = $("#PurchaseProductCode").val();
    var PurchaseManufacturedDate = $("#PurchaseManufacturedDate").val();
    var PurchaseExpireDate = $("#PurchaseExpireDate").val();
    var PurchaseQty = $("#PurchaseQty").val();
    var PurchaseUnitPrice = $("#PurchaseUnitPrice").val();
    var PurchaseTotalPrice = $("#PurchaseTotalPrice").val();
    var PurchasePreviousPrice = $("#PurchasePreviousPrice").val();
    var PurchasePreviousMRP = $("#PurchasePreviousMRP").val();
    var PurchaseNewCostPrice = $("#PurchaseNewCostPrice").val();
    var PurchaseNewMRP = $("#PurchaseNewMRP").val();

    var item = {
        "PurchaseProductId": PurchaseProductId,
        "PurchaseProductCode": PurchaseProductCode,
        "PurchaseManufacturedDate": PurchaseManufacturedDate,
        "PurchaseExpireDate": PurchaseExpireDate,
        "PurchaseQty": PurchaseQty,
        "PurchaseUnitPrice": PurchaseUnitPrice,
        "PurchaseTotalPrice": PurchaseTotalPrice,
        "PurchasePreviousPrice": PurchasePreviousPrice,
        "PurchasePreviousMRP": PurchasePreviousMRP,
        "PurchaseNewCostPrice": PurchaseNewCostPrice,
        "PurchaseNewMRP": PurchaseNewMRP

    }

    return item;

}