﻿$("#AddButton").click(function () {
    CreateRowForSales();
});

function CreateRowForSales() {

    var selectedItem = GetSelectedItem();

    //Check Last Row Index
    var index = $("#SalesDetailsTable").children("tr").length;
    var sl = index;

    //For Model List<Property> Binding For MVC
    var indexTd = "<td style='display:none'><input type='hidden' id='Index" + index + "' name='SalesDetailses.Index' value='" + index + "' /> </td>";

    //For Serial No For UI
    var slTd = "<td id='Sl" + index + "'> " + (++sl) + " </td>";

    var ProductIdTd = "<td> <input type='hidden' id='SalesProductId" + index + "'  name='SalesDetailses[" + index + "].ProductId' value='" + selectedItem.SalesProductId + "' /> " + selectedItem.SalesProductId + " </td>";
    var QuantityTd = "<td> <input type='hidden' id='SalesQuantity" + index + "'  name='SalesDetailses[" + index + "].Quantity' value='" + selectedItem.SalesQuantity + "' /> " + selectedItem.SalesQuantity + " </td>";
    var UnitPriceTd = "<td> <input type='hidden' id='SalesUnitPrice" + index + "'  name='SalesDetailses[" + index + "].UnitPrice' value='" + selectedItem.SalesUnitPrice + "' /> " + selectedItem.SalesUnitPrice + " </td>";
    var TotalPriceTd = "<td> <input type='hidden' id='SalesTotalPrice" + index + "'  name='SalesDetailses[" + index + "].TotalPrice' value='" + selectedItem.SalesTotalPrice + "' /> " + selectedItem.SalesTotalPrice + " </td>";
  

    var newRow = "<tr>" + indexTd + slTd + ProductIdTd + QuantityTd + UnitPriceTd + TotalPriceTd  +  " </tr>";

    $("#SalesDetailsTable").append(newRow);
   

}

function GetSelectedItem() {
    var SalesProductId = $("#SalesProductId").val();
    var SalesQuantity = $("#SalesQuantity").val();
    var SalesUnitPrice = $("#SalesUnitPrice").val();
    var SalesTotalPrice = 0;
    

    var item = {
        "SalesProductId": SalesProductId,
        "SalesQuantity": SalesQuantity,
        "SalesUnitPrice": SalesUnitPrice,
        "SalesTotalPrice": SalesTotalPrice,
       

    }

    return item;

}