﻿
$(document).ready(function () {
   
            var oTable = $('#myDatatable').DataTable({
                "ajax": {
                    "url": "/Category/GetCategories",
                   "type": "get",
                    "datatype": "json"
                },
                "columns": [
                    { "data": "Code", "autoWidth": true },
                    { "data": "Name", "autoWidth": true },
                    {
                        "data": "CategoryId", "width": "50px", "render": function (data) {
                            return '<a class="popup" href="/category/save/'+data+'">Edit</a>';
                        }
                    },
                    {
                        "data": "CategoryId", "width": "50px", "render": function (data) {
                            return '<a class="popup" href="/category/delete/' + data + '">Delete</a>';
                        }
                    }
                    
                ]
            })
            $('.tablecontainer').on('click', 'a.popup', function (e) {
                e.preventDefault();
                OpenPopup($(this).attr('href'));
            })
            function OpenPopup(pageUrl) {
                var $pageContent = $('<div/>');
                $pageContent.load(pageUrl, function () {
                    $('#popupForm', $pageContent).removeData('validator');
                    $('#popupForm', $pageContent).removeData('unobtrusiveValidation');
                    $.validator.unobtrusive.parse('form');

                });

                $dialog = $('<div class="popupWindow" style="overflow:auto"></div>')
                          .html($pageContent)
                          .dialog({
                              draggable: false,
                              autoOpen: false,
                              resizable: false,
                              model: true,
                              title: 'Popup Dialog',
                              height: 550,
                              width: 600,
                              close: function () {
                                  $dialog.dialog('destroy').remove();
                              }
                          })

                $('.popupWindow').on('submit', '#popupForm', function (e) {
                    var url = $('#popupForm')[0].action;
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $(this).serialize(),
                        success: function (data) {
                            if (data.isSaved) {
                                $dialog.dialog('close');
                                oTable.ajax.reload();
                            }
                        }
                    })

                    e.preventDefault();
                })

                $dialog.dialog('open');
            }
        })
    