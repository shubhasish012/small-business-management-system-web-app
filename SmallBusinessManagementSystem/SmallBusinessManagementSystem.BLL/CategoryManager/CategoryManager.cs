﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;
using SmallBusinessManagementSystem.Repository.CategoryRepository;

namespace SmallBusinessManagementSystem.BLL.CategoryManager
{
    public class CategoryManager
    {
        CategoryRepository _repository=new CategoryRepository();
        public List<Category> GetCategories()
        {
            return _repository.GetCategories();
        }

        public Category Save(int id)
        {
            return _repository.Save(id);
        }

        public int SaveByPost(Category category)
        {
            return _repository.SaveByPost(category);
        }

        public Category Delete(int id)
        {
            return _repository.Delete(id);
        }

        public int DeleteCategory(int id)
        {
            return _repository.DeleteCategory(id);
        }
    }
}
