﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;
using SmallBusinessManagementSystem.Repository.SalesRepository;

namespace SmallBusinessManagementSystem.BLL.SalesManager
{
    public class SalesManager
    {
        SalesRepository _repository=new SalesRepository();
        public bool Add(Sales sales)
        {
            return _repository.Add(sales);
        }
    }
}
