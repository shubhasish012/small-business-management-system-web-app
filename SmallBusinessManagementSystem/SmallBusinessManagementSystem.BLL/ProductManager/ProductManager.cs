﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;
using SmallBusinessManagementSystem.Repository.ProductRepository;

namespace SmallBusinessManagementSystem.BLL.ProductManager
{
    public class ProductManager
    {
        ProductRepository _repository=new ProductRepository();
        public List<Product> GetProducts()
        {
            return _repository.GetProducts();
        }

        public Product Save(int id)
        {

            return _repository.Save(id);
        }

        public int SaveByPost(Product product)
        {
            string fileName = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
            string extension = Path.GetExtension(product.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            product.ImagePath = "~/Image/" + fileName;
            fileName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Image/"), fileName);
            product.ImageFile.SaveAs(fileName);
            return _repository.SaveByPost(product);
        }

        public Product Delete(int id)
        {
            return _repository.Delete(id);
        }

        public int DeleteProduct(int id)
        {
            return _repository.DeleteProduct(id);
        }

        public Product GetImageView(int id)
        {
            return _repository.GetImageView(id);
        }

        public object GetCategoryList()
        {
            return _repository.GetCategoryList();
        }
    }
}
