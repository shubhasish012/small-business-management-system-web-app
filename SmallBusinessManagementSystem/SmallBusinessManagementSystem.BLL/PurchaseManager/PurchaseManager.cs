﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;
using SmallBusinessManagementSystem.Repository.PurchaseRepository;

namespace SmallBusinessManagementSystem.BLL.PurchaseManager
{
    public class PurchaseManager
    {
        PurchaseRepository _repository=new PurchaseRepository();
        public bool Add(Purchase purchase)
        {
            return _repository.Add(purchase);
        }
    }
}
