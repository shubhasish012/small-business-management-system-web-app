﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace SmallBusinessManagementSystem.Models.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public int ReorderLevel { get; set; }
        public string Description { get; set; }

        [DisplayName("Upload File")]
        public string ImagePath { get; set; }
        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

      
    }
}