﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallBusinessManagementSystem.Models.Models
{
    public class Purchase
    {
        public int PurchaseId { get; set; }

        public string InvoiceNumber { get; set; }
        public int PurchaseSupplierId { get; set; }

        public DateTime Date { get; set; }

        public virtual ICollection<PurchaseDetails> PurchaseDetailses { get; set; }
    }
}
