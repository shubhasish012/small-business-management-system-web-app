﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallBusinessManagementSystem.Models.Models
{
    public class PurchaseDetails
    {
        public int PurchaseDetailsId { get; set; }

        public int ProductId { get; set; }

        public int ProductCode { get; set; }

        public DateTime ManufacturedDate { get; set; }

        public DateTime ExpireDate { get; set; }

        public double PurchaseQuantity { get; set; }

        public double UnitPrice { get; set; }

        public double TotalPrice { get; set; }

        public double PreviousCostPrice { get; set; }

        public double PreviousMRP { get; set; }

        public double NewCostPrice { get; set; }

        public double NewMRP { get; set; }

        public int PurchaseId { get; set; }

        public virtual Purchase Purchase { get; set; }
    }
}
