﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Build.Framework;

namespace SmallBusinessManagementSystem.Models.Models
{
    public class Category
    {
        public int CategoryId { get; set; }

        [Required]
        [DisplayName("Category Code *")]
        public String Code { get; set; }

        [Required]
        [DisplayName("Category Name *")]
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }

     
    }
}