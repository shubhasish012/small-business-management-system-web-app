﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallBusinessManagementSystem.Models.Models
{
    public class SalesDetails
    {
        public int SalesDetailsId { get; set; }

        public int ProductId { get; set; }

        public double Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double TotalPrice { get; set; }

        public int SalesId { get; set; }

        public virtual Sales Sales { get; set; }


    }
}
