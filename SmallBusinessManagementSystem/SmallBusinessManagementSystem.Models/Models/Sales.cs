﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallBusinessManagementSystem.Models.Models
{
   public class Sales
    {
       public int SalesId { get; set; }

       public int CustomerId { get; set; }

       public DateTime Date { get; set; }

       public int EarnedLoyalityPoint { get; set; }

       public double GrandTotal { get; set; }


       public virtual ICollection<SalesDetails> SalesDetailses { get; set; }


    }
}
