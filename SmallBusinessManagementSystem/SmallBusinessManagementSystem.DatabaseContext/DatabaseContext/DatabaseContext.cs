﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.DatabaseContext.DatabaseContext
{
   public class DatabaseContext:DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<Purchase> Purchases { get; set; }

        public DbSet<PurchaseDetails> PurchaseDetailses { get; set; }

       public DbSet<Sales> Saleses { get; set; }
       public DbSet<SalesDetails> SalesDetailses { get; set; }
    }
}
