﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;
using SmallBusinessManagementSystem.DatabaseContext;


namespace SmallBusinessManagementSystem.Repository.CategoryRepository
{
   public class CategoryRepository
    {
       DatabaseContext.DatabaseContext.DatabaseContext _db=new DatabaseContext.DatabaseContext.DatabaseContext();
       public List<Category> GetCategories()
       {
           var categories = _db.Categories.OrderBy(a => a.Code).ToList();
           return categories;
       }

       public Category Save(int id)
       {
           var category = _db.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
           return category;
       }


       public int SaveByPost(Category category)
       {
           int status = 0;
           if (category.CategoryId > 0)
           {
               //Edit 
               var v = _db.Categories.Where(a => a.CategoryId == category.CategoryId).FirstOrDefault();
               if (v != null)
               {                
                   v.Code = category.Code;
                   v.Name = category.Name;
               }
           }
           else
           {
               //Save
               _db.Categories.Add(category);
           }
           status=_db.SaveChanges();
           return status;

       }

       public Category Delete(int id)
       {
           var category = _db.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
           return category;
       }

       public int DeleteCategory(int id)
       {
           int status = 0;
           var v = _db.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
           if (v != null)
           {
               _db.Categories.Remove(v);
              status= _db.SaveChanges();
               
           }
           return status;
       }
    }
}
