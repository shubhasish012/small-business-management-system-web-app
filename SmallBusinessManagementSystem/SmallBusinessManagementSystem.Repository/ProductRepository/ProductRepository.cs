﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.Repository.ProductRepository
{
    public class ProductRepository
    {
        private DatabaseContext.DatabaseContext.DatabaseContext _db =
            new DatabaseContext.DatabaseContext.DatabaseContext();

        public List<Product> GetProducts()
        {
            var products = _db.Products.OrderBy(a => a.Name).ToList();
            return products;
        }

        public Product Save(int id)
        {
            var product = _db.Products.Where(a => a.ProductId == id).FirstOrDefault();
            return product;
        }

        public int SaveByPost(Product product)
        {
            int status = 0;
            if (product.ProductId > 0)
            {
                //Edit 
                var v = _db.Products.Where(a => a.ProductId == product.ProductId).FirstOrDefault();
                if (v != null)
                {
                    v.Code = product.Code;
                    v.Name = product.Name;
                    v.CategoryId = product.CategoryId;
                    v.ReorderLevel = product.ReorderLevel;
                    v.Description = product.Description;
                    v.ImagePath = product.ImagePath;
                    v.ImageFile = product.ImageFile;
                }
            }
            else
            {
                //Save
                _db.Products.Add(product);
            }
            status = _db.SaveChanges();
            return status;

        }

        public Product Delete(int id)
        {
            var product = _db.Products.Where(a => a.ProductId == id).FirstOrDefault();
            return product;
        }

        public int DeleteProduct(int id)
        {
            int status = 0;
            var v = _db.Products.Where(a => a.ProductId == id).FirstOrDefault();
            if (v != null)
            {
                _db.Products.Remove(v);
                status = _db.SaveChanges();

            }
            return status;
        }

        public Product GetImageView(int id)
        {
            var product = _db.Products.Where(x => x.ProductId == id).FirstOrDefault();
            return product;
        }

        public object GetCategoryList()
        {
            var categories = new SelectList(_db.Categories,"CategoryId","Name");
            return categories;
        }
    }
}
