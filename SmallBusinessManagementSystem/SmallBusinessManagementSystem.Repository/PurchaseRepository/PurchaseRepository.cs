﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallBusinessManagementSystem.Models.Models;

namespace SmallBusinessManagementSystem.Repository.PurchaseRepository
{
    
    public class PurchaseRepository
    {
        DatabaseContext.DatabaseContext.DatabaseContext _db=new DatabaseContext.DatabaseContext.DatabaseContext();
        public bool Add(Purchase purchase)
        {
            int isAdded;
            _db.Purchases.Add(purchase);
            isAdded = _db.SaveChanges();
            if (isAdded > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            

        }
    }
}
